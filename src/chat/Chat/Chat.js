import React from 'react';

import Header from '../Header/Header';
import MessageList from '../MessageList/MessageList';
import MessageInput from '../MessageInput/MessageInput';

import './Chat.css';

let messageData = [];

const DATA_SERVICE_URL = 'https://edikdolynskyi.github.io/react_sources/messages.json';

fetch(DATA_SERVICE_URL)
    .then((response) => {
        return response.json();
    })
    .then((data) => {
        messageData = data;
    })
    .catch((err) => {
        console.log('Error Reading data' + err);
    });

const Chat = () => {    
    return (
        <div className="chat">
            <Header messages={messageData} />
            <MessageList messages={messageData} />
            <MessageInput />
        </div>
    );
};

export default Chat;
