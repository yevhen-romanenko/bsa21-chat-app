import React from 'react';
import dateFormat from 'dateformat';
import classNames from 'classnames';

import './Message.css';

const Message = ({ isMyMessage, message }) => {
    const messageClass = classNames('message-row', {
        'other-message': isMyMessage,
        'own-message': !isMyMessage,
    });

    const image = isMyMessage ? <img src={message.avatar} alt={message.user} /> : null;

    return (
        <div className={messageClass}>
            <div className="message-content">
                {image}
                <div className="message-text">
                    <div className="message-time">{dateFormat(message.createdAt, 'hh:mm')}</div>
                    {message.text}
                    <div className="message-like">
                        <button>
                            {''}
                            <b>Like{''}</b>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Message;
