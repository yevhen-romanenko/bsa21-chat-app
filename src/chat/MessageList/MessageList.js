import React from 'react';
import dateFormat from 'dateformat';

import Message from '../Message/Message';
import Divider from '../Divider/Divider';
import './MessageList.css';

const MessageList = (props) => {
    const messageItems = props.messages.map((message, index) => {
        return <Message key={index} isMyMessage={message.user} message={message}></Message>;
    });

    return (
        <div className="message-list">
            {messageItems}
            <Divider datenow={dateFormat(new Date(), 'mmmm dS')}></Divider>
        </div>
    );
};

export default MessageList;
