import React from 'react';
import dateFormat from 'dateformat';
import classNames from 'classnames';

import './OwnMesage.css';

const OwnMesage = ({ isMyMessage, message }) => {
    const messageClass = classNames('message-row', {
        'other-message': isMyMessage,
        'own-message': !isMyMessage,
    });

    const image = isMyMessage ? <img src={message.avatar} alt={message.user} /> : null;

    return (
        <div className={messageClass}>
            <div className="message-content">
                {image}
                <div className="message-text">
                    <div className="message-time">{dateFormat(message.createdAt, 'hh:mm')}</div>
                    {message.text}
                    <button className=".message-edit">Edit</button>
                    <button className=".message-delete">Delete</button>
                </div>
            </div>
        </div>
    );
};

export default OwnMesage;
