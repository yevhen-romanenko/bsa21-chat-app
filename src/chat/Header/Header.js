import React from 'react';
import dateFormat from 'dateformat';
// import classNames from 'classnames';

import './Header.css';

const Header = (props) => {
    const countMessages = props.messages.filter((message) => message.text).length;

    const Dates = props.messages.map(function (message) {
        return message.createdAt;
    });

    const lastMessageDate = Dates.reduce((a, b) => {
        return a > b ? a : b;
    });

    const countUsers = [...new Set(props.messages.map((message) => message.user))].length;

    return (
        <div className="header">
            <span className="header-title">{'My Chat'}</span>
            <span className="header-users-count">{countUsers} participants</span>
            <span className="header-messages-count">{countMessages} messages</span>
            <span className="header-last-message-date">last message at {dateFormat(lastMessageDate, 'dd.mm.yyyy hh:mm')}</span>
        </div>
    );
};

export default Header;
