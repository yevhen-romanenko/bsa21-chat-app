import React from 'react';

import './MessageInput.css';

class MessageInput extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            message: {
                user: this.user,
                text: this.text,
            },
        };
        this.onSubmit = this.onSubmit.bind(this);
        this.onInputchange = this.onInputchange.bind(this);
    }

    onSubmit = (e) => {
        // e.preventDefault();
        // let text = this.text;
        // console.log(text);
        console.log(this.state);
    };

    onInputchange = (e) => {
        this.setState({
            [e.target.name]: e.target.value,
        });
    };

    render() {
        // const { items } = this.state;
        return (
            <div className="message-input">
                {/* <img src={require("../../images/icons/attachment-logo.svg")} alt="Add Attachment" /> */}
                <input
                    className="message-input-text"
                    type="text"
                    placeholder="Message"
                    value={this.state.text}
                    name="text"
                    onChange={this.onInputchange}
                />
                <button className="message-input-button" id="submit-button" onClick={this.onSubmit}>
                    {' '}
                    &#x2B06; Send{' '}
                </button>
            </div>
        );
    }
}

export default MessageInput;
