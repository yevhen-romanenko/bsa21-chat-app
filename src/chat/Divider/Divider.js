import React from 'react';
import './Divider.css';

const Divider = (props) => {
    return (
        <div className="messages-divider">
            <div className="border" />
            <span className="content">{props.datenow}</span>
            <div className="border" />
        </div>
    );
};

export default Divider;
