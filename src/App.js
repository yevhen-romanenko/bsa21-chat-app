import React from 'react';
import './App.css';

import Chat from './chat/Chat/Chat';

class App extends React.Component {
    state = {
        loading: true,
    };

    componentDidMount() {
        demoAsyncCall().then(() => this.setState({ loading: false }));
    }

    render() {
        const { loading } = this.state;

        if (loading) {
            return null;
        }

        return <Chat></Chat>;
    }
}

function demoAsyncCall() {
    return new Promise((resolve) => setTimeout(() => resolve(), 2500));
}

export default App;
